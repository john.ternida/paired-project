package ph.stacktreck.snakeandladders.ternida.johnkenneth

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.core.content.edit

class PlayerEntryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_player_entry2)


        val player1NameEditText = findViewById<EditText>(R.id.player1NameEditText)
        val player2NameEditText = findViewById<EditText>(R.id.player2NameEditText)

        val backButton = findViewById<Button>(R.id.back_button)
        backButton.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        val playButton = findViewById<Button>(R.id.play_button)
        playButton.setOnClickListener {

            val player1Name = player1NameEditText.text.toString()
            val player2Name = player2NameEditText.text.toString()


            val intent = Intent(this, InGamePageActivity::class.java)
            intent.putExtra("player1Name", player1Name)
            intent.putExtra("player2Name", player2Name)
            startActivity(intent)

        }
    }
}
