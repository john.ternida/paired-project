package ph.stacktreck.snakeandladders.ternida.johnkenneth

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class WinnerPageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_winner_page)
    }
}