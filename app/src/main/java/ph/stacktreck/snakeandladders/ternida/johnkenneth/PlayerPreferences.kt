package ph.stacktreck.snakeandladders.ternida.johnkenneth

import android.content.Context

class PlayerPreferences (context: Context) {
    private val sharedPrefs = context.getSharedPreferences("players", Context.MODE_PRIVATE)

    fun savePlayerNames(player1: String, player2: String) {
        val editor = sharedPrefs.edit()
        editor.putString("player1", player1)
        editor.putString("player2", player2)
        editor.apply()
    }

    fun getPlayerNames(): Pair<String?, String?> {
        val player1 = sharedPrefs.getString("player1", null)
        val player2 = sharedPrefs.getString("player2", null)
        return Pair(player1, player2)
    }
}
